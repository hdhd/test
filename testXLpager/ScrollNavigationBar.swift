//
//  ScrollNavigationBar.swift
//  testXLpager
//
//  Created by NGUYEN HUU DANG on 8/26/16.
//  Copyright © 2016 NGUYEN HUU DANG. All rights reserved.
//

//
//  ScrollNavigationBar.swift
//  Rao Vat JP
//
//  Created by NGUYEN HUU DANG on 8/22/16.
//  Copyright © 2016 Dang. All rights reserved.
//

import UIKit

enum ScrollNavigationBarState: NSInteger {
    case None
    case ScrollUp
    case ScrollDown
}

class ScrollNavigationBar: UINavigationBar {
    
    var state: ScrollNavigationBarState = .None
    var scrollView: UIScrollView!
    var otherView: UIView!
    
    var panGesture: UIPanGestureRecognizer!
    var lastContentOffsetY: CGFloat!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUp()
    }
    
    func setUp() {
        
        panGesture = UIPanGestureRecognizer(target: self, action: #selector(ScrollNavigationBar.handlePan(_:)))
        panGesture.delegate = self
        panGesture.cancelsTouchesInView = false
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ScrollNavigationBar.applicationDidBecomeActive(_:)), name: UIApplicationDidBecomeActiveNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ScrollNavigationBar.statusBarOrientationDidChange(_:)), name: UIApplicationDidChangeStatusBarOrientationNotification, object: nil)
        
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIApplicationDidBecomeActiveNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIApplicationDidChangeStatusBarOrientationNotification, object: nil)
    }
    
    func makeScrollView(sender: UIScrollView!, otherView: UIView!) {
        self.scrollView = sender
        self.otherView = otherView
        if self.panGesture.view != nil {
            self.panGesture.view?.removeGestureRecognizer(self.panGesture)
        }
        if self.scrollView != nil {
            self.scrollView.addGestureRecognizer(self.panGesture)
        }
    }
    
    func resetToDefaultPositionWithAnimation(animated: Bool) {
        
        self.state = .None
        var tempFrame = frame
        tempFrame.origin.y = statusBarTopOffset()
        setFrame(frame: tempFrame, alpha: 1.0, animated: animated)
    }
    
    // MARK: NOTIFICATION
    func statusBarOrientationDidChange(notification: NSNotification) {
        resetToDefaultPositionWithAnimation(false)
    }
    
    func applicationDidBecomeActive(notification: NSNotification) {
        resetToDefaultPositionWithAnimation(false)
    }
    
    // MARK: GESTURE
    func handlePan(gesture: UIPanGestureRecognizer) {
        
        if self.scrollView == nil || gesture.view != self.scrollView {
            return
        }
        
        if self.scrollView.frame.size.height + self.bounds.size.height * 2 >= self.scrollView.contentSize.height {
            return
        }
        
        let contentOffsetY = self.scrollView.contentOffset.y
        
        if gesture.state == .Began {
            self.state = .None
            self.lastContentOffsetY = contentOffsetY
            return
        }
        
        let deltaY = contentOffsetY - self.lastContentOffsetY
        if deltaY < 0 {
            self.state = .ScrollDown
        } else if deltaY > 0 {
            self.state = .ScrollUp
        }
        
        var tempFrame = self.frame
        var alpha: CGFloat = 1.0
        let maxY = self.statusBarTopOffset()
        let minY = maxY - CGRectGetHeight(tempFrame) + 1.0
        
        let contentInsetTop = self.scrollView.contentInset.top
        let isBouncePastTopEdge = contentOffsetY < -contentInsetTop
        if isBouncePastTopEdge && CGRectGetMinY(tempFrame) == maxY {
            self.lastContentOffsetY = contentOffsetY
            return
        }
        
        let isScrolling = (state == .ScrollUp || state == .ScrollDown)
        let gestureIsActive = (gesture.state != .Ended && gesture.state != .Cancelled)
        
        if isScrolling && !gestureIsActive {
            if self.state == .ScrollDown {
                tempFrame.origin.y = maxY
                alpha = 1
            } else if self.state == .ScrollUp {
                tempFrame.origin.y = minY
                alpha = 0.000001
            }
            setFrame(frame: tempFrame, alpha: alpha, animated: true)
        } else {
            tempFrame.origin.y -= deltaY
            tempFrame.origin.y = min(maxY, max(tempFrame.origin.y, minY))
            alpha = (tempFrame.origin.y - (minY + maxY))/(-minY)
            alpha = max(0.000001, alpha)
            setFrame(frame: tempFrame, alpha: alpha, animated: false)
        }
        
        if isBouncePastTopEdge && CGRectGetMinY(tempFrame) != maxY {
            
        } else {
            lastContentOffsetY = contentOffsetY
        }
    }
    
    func statusBarTopOffset() ->CGFloat {
        
        let statusBarFrame = UIApplication.sharedApplication().statusBarFrame
        
        var topOffset = min(CGRectGetMaxX(statusBarFrame), CGRectGetMaxY(statusBarFrame))
        let isInCallStatusBar = topOffset == 40
        if isInCallStatusBar {
            topOffset -= 20
        }
        return topOffset
    }
    
    func setFrame(frame frame: CGRect, alpha: CGFloat, animated: Bool) {
        
        if animated {
            UIView.beginAnimations("ScrollNavigationBarAnimation", context: nil)
        }
        
        let offsetY = CGRectGetMinY(frame) - CGRectGetMinY(self.frame)
        
        for (index,view) in self.subviews.enumerate() {
            let isBackgroundView = index == 0
            let isViewHidden = view.hidden || view.alpha == 0
            if isBackgroundView || isViewHidden {
                continue
            }
            view.alpha = alpha
        }
        self.frame = frame
        
        
        
        if self.otherView != nil {
            var tempFrame = otherView.frame
            tempFrame.origin.y += offsetY
            tempFrame.size.height -= offsetY
            self.otherView.frame = tempFrame
        }
        
        if animated {
            UIView.commitAnimations()
        }
        
    }
    
    
}

extension ScrollNavigationBar: UIGestureRecognizerDelegate {
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}